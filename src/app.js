import '../assets/styles/app.scss';

import { h, render } from 'preact';
import { Router, Link } from 'preact-router';
import {useState, useEffect} from 'preact/hooks';

import {findStorages} from './api/api';
import {findProducts } from './api/api';
import Home from './pages/home';
import Storage from './pages/storage';

function App() {
    const [storages, setStorages] = useState(null);
    const [products, setProducts] = useState(null);

    useEffect(() => {
        findStorages().then((storages) => setStorages(storages));
        findProducts().then(products => setProducts(products));
    }, []);

    if (storages === null) {
        return (
            <div>
                <div className="text-center pt-5">Loading...</div>
                <div>ENV_API_ENDPOINT: {ENV_API_ENDPOINT}</div>
            </div>
        );
    }

    return (
        <div>
            <header className="header">
                <nav className="navbar navbar-light bg-light">
                    <div className="container">
                        <Link className="navbar-brand mr-4 pr-2" href="/">
                            &#128217; My Home Storage
                        </Link>
                    </div>
                </nav>

                <nav className="bg-light border-bottom text-center">
                    {storages.map((storage) => (
                        <Link className="nav-conference" href={'/storage/'+storage.id}>
                            {storage.name}
                        </Link>
                        ))}
                </nav>
            </header>

            <Router>
                <Home path="/" products={products} />
                <Storage path="/storage/:id" storages={storages} />
            </Router>
        </div>
    )
}

render(<App />, document.getElementById('app'));