import { h } from 'preact';

export default function Statistic({ statistics }) {
    if (statistics !== null && statistics.length === 0) {
        return <div className="text-center pt-4">No statistics yet</div>;
    }

    if (!statistics) {
        return <div className="text-center pt-4">Loading...</div>;
    }

    return (
        <div className="pt-4">
            <div className="">
                <span class="border p-3 mb-4">expired: { statistics.expired }</span> 
                <span class="border p-3 mb-4">expiring soon: { statistics.expiring_soon }</span> 
                <span class="border p-3 mb-4">not expired: { statistics.not_expired }</span> 
                <span class="border p-3 mb-4">total: { statistics.total }</span> 
            </div>
        </div>
    );
}