import { h } from 'preact';
import { findStorageProducts } from '../api/api';
import { findStorageStatistics } from '../api/api';
import { useState, useEffect } from 'preact/hooks';
import Product from './product';
import Statistic from './statistic';

// function Product({ products }) {
//     if (products !== null && products.length === 0) {
//         return <div className="text-center pt-4">No products yet</div>;
//     }

//     if (!products) {
//         return <div className="text-center pt-4">Loading...</div>;
//     }

//     return (
//         <div className="pt-4">
//             {products.map(product => (
//                 <div className="shadow border rounded-lg p-3 mb-4">
//                     <h5 className="font-weight-light mt-3 mb-0">{product.description}</h5>
//                     <div className="comment-text">{product.quantity}</div>
//                     <div className="comment-text">{product.expiry_date}</div>
//                 </div>
//             ))}
//         </div >
//     );
// }

export default function Storage({ storages, id }) {
    const storage = storages.find(storage => storage.id === parseInt(id));
    const [products, setProducts] = useState(null);
    const [statistics, setStatistics] = useState(null);

    useEffect(() => {
        findStorageProducts(storage).then(products => setProducts(products));
        findStorageStatistics(storage).then(statistics => setStatistics(statistics));
    }, [id]);

    return (
        <div className="p-3">
            <h4>{storage.name}</h4>
            <Statistic statistics={statistics} />
            <Product products={products} />
        </div>
    );
}