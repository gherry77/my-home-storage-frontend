import { h } from 'preact';

// export default function Product() {
//     return (
//         <div>Product</div>
//     );
// };

export default function Product({ products }) {
    if (products !== null && products.length === 0) {
        return <div className="text-center pt-4">No products yet</div>;
    }

    if (!products) {
        return <div className="text-center pt-4">Loading...</div>;
    }

    return (
        <div className="pt-4">
            {/* {products.map(product => (
                <div className="shadow border rounded-lg p-3 mb-4">
                    <h5 className="font-weight-light mt-3 mb-0">{product.description}</h5>
                    <div className="comment-text">{product.quantity}</div>
                    <div className="comment-text">{product.expiry_date}</div>
                </div>
            ))} */}

            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Description</th>
                        <th scope="col">Expiring Days</th>
                        <th scope="col">Quantity</th>
                    </tr>
                </thead>
                <tbody>
                    {products.map((product) => (
                        <tr>
                            <td>{product.description}</td>
                            <td>{product.expiring_days}</td>
                            <td>{product.quantity}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div >
    );
}