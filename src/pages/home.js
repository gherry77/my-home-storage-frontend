import { h } from 'preact';
import { Link } from 'preact-router';
// import { findStorageStatistics } from '../api/api';
// import { useState, useEffect } from 'preact/hooks';
import Product from './product';

// function Statistic( props ) {
//     const storage = props.storage;

//     console.log(props.storage);

//     if (!storage) {
//         return (
//             <div>
//                 <div className="p-3 text-center">No storage </div>
//             </div>
//         );
//     }
//     const statistic = findStorageStatistics(storage);

//     if (!statistic) {
//         return (
//             <div>
//                 <div className="p-3 text-center">No statistic yet</div>
//             </div>
//         );
//     }

//     return (
//         <div className="pt-4">
//             <h5 className="font-weight-light mt-3 mb-0">{statistic.expired}</h5>
//             <div className="comment-text">{statistic.expiring_soon}</div>
//             <div className="comment-text">{statistic.not_expired}</div>
//             <div className="comment-text">{statistic.total}</div>
//         </div >
//     );
// }

export default function Home({ products }) {

    // ### STORAGE ###

    // if (!storages) {
    //     return (
    //         <div>
    //             <div className="p-3 text-center">No storages yet
    //                 <br/> ENV_API_ENDPOINT: {ENV_API_ENDPOINT}
    //             </div>
    //         </div>
    //     );
    // }

    // return (
    //     <div className="p-3">
    //         {storages.map((storage)=> (
    //             <div className="card border shadow-sm lift mb-3">
    //                 <div className="card-body">
    //                     <div className="card-title">
    //                         <h4 className="font-weight-light">
    //                             {storage.name}
    //                         </h4>
    //                     </div>

    //                     <Link className="btn btn-sm btn-blue stretched-link" href={'/storage/'+storage.id}>
    //                         View
    //                     </Link>
    //                 </div>
    //             </div>
    //         ))}
    //     </div>

    // );

    // ### STATISTICS ###

    // return (
    //     <div className="p-3">
    //         {storages.map((storage)=> (
    //             <div>
    //                 <h4>{storage.name} <span class="badge badge-danger">Danger</span><span class="badge badge-light">9</span></h4>
    //                 <Statistic storage={storage} />

    //             </div>
    //         ))}
    //     </div>
    // );

    // ### Products ###
    return (
        <div className="p-3">
            <Product products={products} />
        </div>
    );
}